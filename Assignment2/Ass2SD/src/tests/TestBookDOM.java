package tests;

import static org.junit.Assert.*;

import java.util.LinkedList;

import org.junit.Test;

import dom.BookDOM;
import mvc.model.BookModel;

public class TestBookDOM {

	BookDOM edom = new BookDOM();

	@Test
	public void testAddBook() {
		assertFalse(edom.searchForBookByAuthor("test") != null);
		edom.addBook("2", "test", "test", "test", "test", "test");
		assertTrue(edom.searchForBookByAuthor("test") != null);
	}

	@Test
	public void testDeleteBook() {

		edom.deletebook("2");
		assertFalse(edom.searchForBookByAuthor("test") != null);
	}

	@Test
	public void testUpdateBook() {
		edom.addBook("2", "test", "test", "test", "test", "test");
		edom.updateBook("2", "test2", "test2");
		edom.deletebook("2");
	}

	@Test
	public void testViewAllBooks() {
		LinkedList<BookModel> list = (LinkedList<BookModel>) edom.viewAllbooks();

		assertTrue(list != null);

		for (int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i).toString());
		}
	}

}
