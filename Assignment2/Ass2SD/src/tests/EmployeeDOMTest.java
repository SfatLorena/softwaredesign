package tests;

import static org.junit.Assert.*;

import java.util.LinkedList;

import org.junit.Test;

import dom.EmployeeDOM;
import mvc.model.EmployeeModel;

public class EmployeeDOMTest {
	EmployeeDOM  edom= new EmployeeDOM();

	@Test
	public void testAddEmployee() {
		assertFalse(edom.searchEmployeeUsername("carla"));
		edom.addEmployee("carla", "123", "Brejea", "Carla", "Zalau");
		assertTrue(edom.searchEmployeeUsername("carla"));
	}

	@Test
	public void testDeleteEmployee(){
		
		edom.deleteEmployee("carla");
		assertFalse(edom.searchEmployeeUsername("carla"));
	}
	
	@Test
	public void testUpdateEmployee(){
		edom.updateEmployee("ade", "sfat", "adelinana", "Zalau");
	}
	@Test
	public void testViewAllEmployees(){
		LinkedList<EmployeeModel> list = (LinkedList<EmployeeModel>) edom.viewAllEmployees();
		
		assertTrue(list!=null);
		
		for(int i = 0; i<list.size(); i++){
			System.out.println(list.get(i).toString());
		}
	}
}
