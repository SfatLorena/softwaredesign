package dom;

import java.util.LinkedList;
import java.util.List;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import mvc.model.BookModel;

public class BookDOM {
	// title, author, genre, quantity, and price
	public void addBook(String idIN, String titleIN, String authorIN, String genreIN, String quantityIN,
			String priceIN) {
		try {
			Document d = DOMhelper.getDocument("src\\data\\books.xml");
			Element books = d.getDocumentElement();

			// Create book tag
			Element book = d.createElement("book");

			// create id tag
			Element id = d.createElement("id");
			id.appendChild(d.createTextNode(idIN));
			book.appendChild(id);

			// create title tag
			Element title = d.createElement("title");
			title.appendChild(d.createTextNode(titleIN));
			book.appendChild(title);

			// create author tag
			Element author = d.createElement("author");
			author.appendChild(d.createTextNode(authorIN));
			book.appendChild(author);

			// create genre tag
			Element genre = d.createElement("genre");
			genre.appendChild(d.createTextNode(genreIN));
			book.appendChild(genre);

			// create quantity tag
			Element quantity = d.createElement("quantity");
			quantity.appendChild(d.createTextNode(quantityIN));
			book.appendChild(quantity);

			// create price tag
			Element price = d.createElement("price");
			price.appendChild(d.createTextNode(priceIN));
			book.appendChild(price);

			books.appendChild(book);

			// Write to file
			DOMhelper.saveXMLContent(d, "src\\data\\books.xml");

		} catch (Exception e) {
			System.out.println("[BookDOM]Error Adding a new book!");
		}
		System.out.println("[BookDOM]Added an book!");
	}

	public void deletebook(String id) {
		try {
			Document d = DOMhelper.getDocument("src\\data\\books.xml");
			NodeList nl = d.getElementsByTagName("book");
			for (int i = 0; i < nl.getLength(); i++) {
				Element book = (Element) nl.item(i);
				if (book.getElementsByTagName("id").item(0).getTextContent().equals(id))
					book.getParentNode().removeChild(book);
			}
			// Write to file
			DOMhelper.saveXMLContent(d, "src\\data\\books.xml");

		} catch (Exception e) {
			System.out.println("[bookDOM]Error Deleting an book!");
		}
		System.out.println("[bookDOM]Deleted an book!");
	}

	public void updateBook(String id, String quantityIN, String priceIN) {

		try {
			Document d = DOMhelper.getDocument("src\\data\\books.xml");

			NodeList nl = d.getElementsByTagName("book");
			for (int i = 0; i < nl.getLength(); i++) {
				Element book = (Element) nl.item(i);
				if (book.getElementsByTagName("id").item(0).getTextContent().equals(id)) {

					System.out.println("Book data to be added: " + id + " " + quantityIN + " " + priceIN);

					if (!quantityIN.equals("")) {
						book.getElementsByTagName("quantity").item(0).setTextContent(quantityIN);
						System.out.println("Book quantity changed!");
					}
					if (!priceIN.equals("")) {
						book.getElementsByTagName("price").item(0).setTextContent(priceIN);
						System.out.println("Book price changed!");
					}
				}
			}
			// Write to file
			DOMhelper.saveXMLContent(d, "src\\data\\books.xml");
		} catch (Exception e) {
			System.out.println("[bookDOM]Error Updating a book!");
		}
		System.out.println("[bookDOM]Updated a book!");
	}

	public String getLastID() {
		String id;
		try {
			Document d = DOMhelper.getDocument("src\\data\\books.xml");
			XPath xp = XPathFactory.newInstance().newXPath();
			NodeList nl = (NodeList) xp.compile("//book").evaluate(d, XPathConstants.NODESET);
			System.out.println("[Starting to extract!]");

			id = xp.compile("./id").evaluate(nl.item(nl.getLength() - 1));

		} catch (Exception e) {
			System.out.println("[bookDOM]Error getting last ID of books!");
			return null;
		}
		return id;
	}

	public List<BookModel> viewAllbooks() {
		LinkedList<BookModel> list = new LinkedList<BookModel>();
		BookModel empl;
		String id, title, author, genre, quantity, price;
		try {
			Document d = DOMhelper.getDocument("src\\data\\books.xml");
			XPath xp = XPathFactory.newInstance().newXPath();
			NodeList nl = (NodeList) xp.compile("//book").evaluate(d, XPathConstants.NODESET);
			System.out.println("[Starting to extract!]");
			for (int i = 0; i < nl.getLength(); i++) {

				empl = new BookModel();

				System.out.println("[Extracting for " + i + "!]");
				id = xp.compile("./id").evaluate(nl.item(i));
				title = xp.compile("./title").evaluate(nl.item(i));
				author = xp.compile("./author").evaluate(nl.item(i));
				genre = xp.compile("./genre").evaluate(nl.item(i));
				quantity = xp.compile("./quantity").evaluate(nl.item(i));
				price = xp.compile("./price").evaluate(nl.item(i));

				System.out.println("[Data extracted!]" + title + " by " + author);

				try {
					empl.setID(Integer.parseInt(id));
				} catch (NumberFormatException e) {
					System.out.println("[Error] parsing!");
				}
				empl.setAuthor(author);
				empl.setGenre(genre);
				try {
					empl.setPrice(Double.parseDouble(price));
				} catch (NumberFormatException e) {
					System.out.println("[Error] parsing!");
				}
				empl.setTitle(title);
				try {
					empl.setQuantity(Integer.parseInt(quantity));
				} catch (NumberFormatException e) {
					System.out.println("[Error] parsing!");
				}
				System.out.println("[bookModel set!]");

				list.add(empl);
				System.out.println("[Added!]");
			}

		} catch (Exception e) {
			System.out.println("[bookDOM]Error viewing list of books!");
			return null;
		}
		return list;
	}

	public List<BookModel> searchForBookByTitle(String title) {

		LinkedList<BookModel> list = new LinkedList<BookModel>();
		BookModel empl;
		String id, tittle, author, genre, quantity, price;
		try {
			Document d = DOMhelper.getDocument("src\\data\\books.xml");
			XPath xp = XPathFactory.newInstance().newXPath();
			NodeList nl = (NodeList) xp.compile("//book").evaluate(d, XPathConstants.NODESET);
			System.out.println("[Starting to extract!]");
			for (int i = 0; i < nl.getLength(); i++) {
				Element book = (Element) nl.item(i);
				if (book.getElementsByTagName("title").item(0).getTextContent().equals(title)) {
					empl = new BookModel();

					System.out.println("[Extracting for " + i + "!]");
					id = xp.compile("./id").evaluate(nl.item(i));
					tittle = xp.compile("./title").evaluate(nl.item(i));
					author = xp.compile("./author").evaluate(nl.item(i));
					genre = xp.compile("./genre").evaluate(nl.item(i));
					quantity = xp.compile("./quantity").evaluate(nl.item(i));
					price = xp.compile("./price").evaluate(nl.item(i));

					System.out.println("[Data extracted!]" + tittle + " by " + author);

					try {
						empl.setID(Integer.parseInt(id));
					} catch (NumberFormatException e) {
						System.out.println("[Error] parsing!");
					}
					empl.setAuthor(author);
					empl.setGenre(genre);
					try {
						empl.setPrice(Double.parseDouble(price));
					} catch (NumberFormatException e) {
						System.out.println("[Error] parsing!");
					}
					empl.setTitle(title);
					try {
						empl.setQuantity(Integer.parseInt(quantity));
					} catch (NumberFormatException e) {
						System.out.println("[Error] parsing!");
					}
					System.out.println("[bookModel set!]");

					list.add(empl);
					System.out.println("[Added!]");
				}
			}

		} catch (Exception e) {
			System.out.println("[bookDOM]Error viewing list of books by title!");
			return null;
		}
		return list;

	}

	public List<BookModel> searchForBookByGenre(String genre) {
		LinkedList<BookModel> list = new LinkedList<BookModel>();
		BookModel empl;
		String id, tittle, author, genrre, quantity, price;
		try {
			Document d = DOMhelper.getDocument("src\\data\\books.xml");
			XPath xp = XPathFactory.newInstance().newXPath();
			NodeList nl = (NodeList) xp.compile("//book").evaluate(d, XPathConstants.NODESET);
			System.out.println("[Starting to extract!]");
			for (int i = 0; i < nl.getLength(); i++) {
				Element book = (Element) nl.item(i);
				if (book.getElementsByTagName("genre").item(0).getTextContent().equals(genre)) {
					empl = new BookModel();

					System.out.println("[Extracting for " + i + "!]");
					id = xp.compile("./id").evaluate(nl.item(i));
					tittle = xp.compile("./title").evaluate(nl.item(i));
					author = xp.compile("./author").evaluate(nl.item(i));
					genrre = xp.compile("./genre").evaluate(nl.item(i));
					quantity = xp.compile("./quantity").evaluate(nl.item(i));
					price = xp.compile("./price").evaluate(nl.item(i));

					System.out.println("[Data extracted!]" + tittle + " by " + author);

					try {
						empl.setID(Integer.parseInt(id));
					} catch (NumberFormatException e) {
						System.out.println("[Error] parsing!");
					}
					empl.setAuthor(author);
					empl.setGenre(genre);
					try {
						empl.setPrice(Double.parseDouble(price));
					} catch (NumberFormatException e) {
						System.out.println("[Error] parsing!");
					}
					empl.setTitle(tittle);
					try {
						empl.setQuantity(Integer.parseInt(quantity));
					} catch (NumberFormatException e) {
						System.out.println("[Error] parsing!");
					}
					System.out.println("[bookModel set!]");

					list.add(empl);
					System.out.println("[Added!]");
				}
			}

		} catch (Exception e) {
			System.out.println("[bookDOM]Error viewing list of books by title!");
			return null;
		}
		return list;
	}
	
	public LinkedList<BookModel> getBooksOutOfStock(){
		LinkedList<BookModel> list = new LinkedList<BookModel>();
		BookModel empl;
		String id, tittle, autthor, genre, quantity, price;
		try {
			Document d = DOMhelper.getDocument("src\\data\\books.xml");
			XPath xp = XPathFactory.newInstance().newXPath();
			NodeList nl = (NodeList) xp.compile("//book").evaluate(d, XPathConstants.NODESET);
			System.out.println("[Starting to extract!]");
			for (int i = 0; i < nl.getLength(); i++) {
				Element book = (Element) nl.item(i);
				if (book.getElementsByTagName("quantity").item(0).getTextContent().equals("0")) {
					empl = new BookModel();

					System.out.println("[Extracting for " + i + "!]");
					id = xp.compile("./id").evaluate(nl.item(i));
					tittle = xp.compile("./title").evaluate(nl.item(i));
					autthor = xp.compile("./author").evaluate(nl.item(i));
					genre = xp.compile("./genre").evaluate(nl.item(i));
					quantity = xp.compile("./quantity").evaluate(nl.item(i));
					price = xp.compile("./price").evaluate(nl.item(i));


					try {
						empl.setID(Integer.parseInt(id));
					} catch (NumberFormatException e) {
						System.out.println("[Error] parsing!");
					}
					empl.setAuthor(autthor);
					empl.setGenre(genre);
					try {
						empl.setPrice(Double.parseDouble(price));
					} catch (NumberFormatException e) {
						System.out.println("[Error] parsing!");
					}
					empl.setTitle(tittle);
					try {
						empl.setQuantity(Integer.parseInt(quantity));
					} catch (NumberFormatException e) {
						System.out.println("[Error] parsing!");
					}
					System.out.println("[bookModel set!]");

					list.add(empl);
					System.out.println("[Added!]");
				}
			}

		} catch (Exception e) {
			System.out.println("[bookDOM]Error viewing list of books by title!");
			return null;
		}
		return list;
	}

	public LinkedList<BookModel> searchForBookByAuthor(String author) {
		LinkedList<BookModel> list = new LinkedList<BookModel>();
		BookModel empl;
		String id, tittle, autthor, genre, quantity, price;
		try {
			Document d = DOMhelper.getDocument("src\\data\\books.xml");
			XPath xp = XPathFactory.newInstance().newXPath();
			NodeList nl = (NodeList) xp.compile("//book").evaluate(d, XPathConstants.NODESET);
			System.out.println("[Starting to extract!]");
			for (int i = 0; i < nl.getLength(); i++) {
				Element book = (Element) nl.item(i);
				if (book.getElementsByTagName("author").item(0).getTextContent().equals(author)) {
					empl = new BookModel();

					System.out.println("[Extracting for " + i + "!]");
					id = xp.compile("./id").evaluate(nl.item(i));
					tittle = xp.compile("./title").evaluate(nl.item(i));
					autthor = xp.compile("./author").evaluate(nl.item(i));
					genre = xp.compile("./genre").evaluate(nl.item(i));
					quantity = xp.compile("./quantity").evaluate(nl.item(i));
					price = xp.compile("./price").evaluate(nl.item(i));

					System.out.println("[Data extracted!]" + tittle + " by " + author);

					try {
						empl.setID(Integer.parseInt(id));
					} catch (NumberFormatException e) {
						System.out.println("[Error] parsing!");
					}
					empl.setAuthor(author);
					empl.setGenre(genre);
					try {
						empl.setPrice(Double.parseDouble(price));
					} catch (NumberFormatException e) {
						System.out.println("[Error] parsing!");
					}
					empl.setTitle(tittle);
					try {
						empl.setQuantity(Integer.parseInt(quantity));
					} catch (NumberFormatException e) {
						System.out.println("[Error] parsing!");
					}
					System.out.println("[bookModel set!]");

					list.add(empl);
					System.out.println("[Added!]");
				}
			}

		} catch (Exception e) {
			System.out.println("[bookDOM]Error viewing list of books by title!");
			return null;
		}
		return list;
	}

	public BookModel getBook(String id) {
		try {
			Document d = DOMhelper.getDocument("src\\data\\books.xml");
			BookModel bookm;
			NodeList nl = d.getElementsByTagName("book");
			for (int i = 0; i < nl.getLength(); i++) {
				Element book = (Element) nl.item(i);
				if (book.getElementsByTagName("id").item(0).getTextContent().equals(id)) {
					bookm = new BookModel();
					bookm.setAuthor(book.getElementsByTagName("author").item(0).getTextContent());
					bookm.setGenre(book.getElementsByTagName("genre").item(0).getTextContent());
					bookm.setID(Integer.parseInt(book.getElementsByTagName("id").item(0).getTextContent()));
					bookm.setPrice(Double.parseDouble(book.getElementsByTagName("price").item(0).getTextContent()));
					bookm.setQuantity(Integer.parseInt(book.getElementsByTagName("quantity").item(0).getTextContent()));
					bookm.setTitle(book.getElementsByTagName("title").item(0).getTextContent());
					return bookm;
				}
			}
			// Write to file
			DOMhelper.saveXMLContent(d, "src\\data\\books.xml");
		} catch (Exception e) {
			System.out.println("[bookDOM]Error Finding a book!");
		}
		System.out.println("[bookDOM]Failed to find a book!");
		return null;
	}
}
