package dom;

import org.w3c.dom.*;

import mvc.model.EmployeeModel;

import java.util.LinkedList;
import java.util.List;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

public class EmployeeDOM {
	public void addEmployee(String usernameIN, String passwordIN,
			String nameIN, String surnameIN, String addressIN){
		try{
			Document d = DOMhelper.getDocument("src\\data\\employees.xml");
			Element employees = d.getDocumentElement();
			
			//Create Employee tag
			Element employee = d.createElement("employee");
			
			//create Username tag
			Element username = d.createElement("username");
			username.appendChild(d.createTextNode(usernameIN));
			employee.appendChild(username);
			
			//create Password tag
			Element password = d.createElement("password");
			password.appendChild(d.createTextNode(passwordIN));
			employee.appendChild(password);

			//create Name tag
			Element name = d.createElement("name");
			name.appendChild(d.createTextNode(nameIN));
			employee.appendChild(name);

			//create Surname tag
			Element surname = d.createElement("surname");
			surname.appendChild(d.createTextNode(surnameIN));
			employee.appendChild(surname);

			//create Address tag
			Element address = d.createElement("address");
			address.appendChild(d.createTextNode(addressIN));
			employee.appendChild(address);
			
			employees.appendChild(employee);
			
			//Write to file
			DOMhelper.saveXMLContent(d, "src\\data\\employees.xml");

		}catch(Exception e){
			System.out.println("[EmployeeDOM]Error Adding a new Employee!");
		}
		System.out.println("[EmployeeDOM]Added an employee!");
	}
	
	public boolean deleteEmployee(String username){
		try{
			Document d = DOMhelper.getDocument("src\\data\\employees.xml");
			NodeList nl = d.getElementsByTagName("employee");
			for(int i = 0; i<nl.getLength(); i++){
				Element employee = (Element)nl.item(i);
				if(employee.getElementsByTagName("username").item(0).getTextContent().equals(username))
					employee.getParentNode().removeChild(employee);
			}
		//Write to file
		DOMhelper.saveXMLContent(d, "src\\data\\employees.xml");

		}catch(Exception e){
			System.out.println("[EmployeeDOM]Error Deleting an Employee!");
			return false;
		}
		System.out.println("[EmployeeDOM]Deleted an Employee!");
		return true;
	}
	
	public void updateEmployee(String username,String nameIN, String surnameIN, String addressIN){
		
		try{
			Document d = DOMhelper.getDocument("src\\data\\employees.xml");

			NodeList nl = d.getElementsByTagName("employee");
			for(int i = 0; i<nl.getLength(); i++){
				Element employee = (Element)nl.item(i);
				if(employee.getElementsByTagName("username").item(0).getTextContent().equals(username)){
					
					employee.getElementsByTagName("name").item(0).setTextContent(nameIN);
					employee.getElementsByTagName("surname").item(0).setTextContent(surnameIN);
					employee.getElementsByTagName("address").item(0).setTextContent(addressIN);
				}	
			}
			//Write to file
			DOMhelper.saveXMLContent(d, "src\\data\\employees.xml");
		}catch(Exception e){
			System.out.println("[EmployeeDOM]Error Updating an Employee!");
		}
		System.out.println("[EmployeeDOM]Updated an Employee!");
	}
	
	public List<EmployeeModel> viewAllEmployees(){
		LinkedList<EmployeeModel> list = new LinkedList<EmployeeModel>();
		EmployeeModel empl;
		String username, password, name, surname, address;
		try{
			Document d = DOMhelper.getDocument("src\\data\\employees.xml");
			XPath xp = XPathFactory.newInstance().newXPath();
			NodeList nl = (NodeList)xp.compile("//employee").evaluate(d, XPathConstants.NODESET);
			System.out.println("[Starting to extract!]");
			for(int i = 0; i<nl.getLength(); i++){
				
				empl = new EmployeeModel();
				
				System.out.println("[Extracting for "+i+"!]");
				username = xp.compile("./username").evaluate(nl.item(i));
				password = xp.compile("./password").evaluate(nl.item(i));
				name = xp.compile("./name").evaluate(nl.item(i));
				surname = xp.compile("./surname").evaluate(nl.item(i));
				address = xp.compile("./address").evaluate(nl.item(i));

				System.out.println("[Data extracted!]");
				
				empl.setName(name);
				empl.setSurname(surname);
				empl.setAddress(address);
				empl.setUsername(username);
				empl.setPassword(password);
				System.out.println("[EmployeeModel set!]");
				
				list.add(empl);
				System.out.println("[Added!]");
			}
			
		}catch(Exception e){
			System.out.println("[EmployeeDOM]Error viewing list of Employees!");
			return null;
		}
		return list;
	}
	
	public boolean loginEmployee(String username, String password){
		try{
			Document d = DOMhelper.getDocument("src\\data\\employees.xml");

			NodeList nl = d.getElementsByTagName("employee");
			for(int i = 0; i<nl.getLength(); i++){
				Element employee = (Element)nl.item(i);
				if(employee.getElementsByTagName("username").item(0).getTextContent().equals(username) && 
						employee.getElementsByTagName("password").item(0).getTextContent().equals(password)){
					System.out.println("===== Employee found!");
					return true;
				}	
			}
			//Write to file
			DOMhelper.saveXMLContent(d, "src\\data\\employees.xml");
		}catch(Exception e){
			System.out.println("[EmployeeDOM]Error Finding an Employee!");
		}
		System.out.println("[EmployeeDOM]Failed to find an Employee!");
		return false;
	}
	
	public boolean searchEmployeeUsername(String username){
		try{
			Document d = DOMhelper.getDocument("src\\data\\employees.xml");

			NodeList nl = d.getElementsByTagName("employee");
			for(int i = 0; i<nl.getLength(); i++){
				Element employee = (Element)nl.item(i);
				if(employee.getElementsByTagName("username").item(0).getTextContent().equals(username)){
					System.out.println("===== Employee found!");
					return true;
				}	
			}
			//Write to file
			DOMhelper.saveXMLContent(d, "src\\data\\employees.xml");
		}catch(Exception e){
			System.out.println("[EmployeeDOM]Error Finding an Employee!");
		}
		System.out.println("[EmployeeDOM]Failed to find an Employee!");
		return false;
	}
	
	public EmployeeModel getEmployee(String username, String password){
		try{
			Document d = DOMhelper.getDocument("src\\data\\employees.xml");
			EmployeeModel empl;
			NodeList nl = d.getElementsByTagName("employee");
			for(int i = 0; i<nl.getLength(); i++){
				Element employee = (Element)nl.item(i);
				if(employee.getElementsByTagName("username").item(0).getTextContent().equals(username) && 
						employee.getElementsByTagName("password").item(0).getTextContent().equals(password)){
					System.out.println("===== Employee found!");
					empl = new EmployeeModel();
					empl.setAddress(employee.getElementsByTagName("address").item(0).getTextContent());
					empl.setName(employee.getElementsByTagName("name").item(0).getTextContent());
					empl.setPassword(employee.getElementsByTagName("password").item(0).getTextContent());
					empl.setSurname(employee.getElementsByTagName("surname").item(0).getTextContent());
					empl.setUsername(username);
					return empl;
				}	
			}
			//Write to file
			DOMhelper.saveXMLContent(d, "src\\data\\employees.xml");
		}catch(Exception e){
			System.out.println("[EmployeeDOM]Error Finding an Employee!");
		}
		System.out.println("[EmployeeDOM]Failed to find an Employee!");
		return null;
	}
	
	public EmployeeModel getEmployee(String username){
		try{
			Document d = DOMhelper.getDocument("src\\data\\employees.xml");
			EmployeeModel empl;
			NodeList nl = d.getElementsByTagName("employee");
			for(int i = 0; i<nl.getLength(); i++){
				Element employee = (Element)nl.item(i);
				if(employee.getElementsByTagName("username").item(0).getTextContent().equals(username)){
					System.out.println("===== Employee found!");
					empl = new EmployeeModel();
					empl.setAddress(employee.getElementsByTagName("address").item(0).getTextContent());
					empl.setName(employee.getElementsByTagName("name").item(0).getTextContent());
					empl.setPassword(employee.getElementsByTagName("password").item(0).getTextContent());
					empl.setSurname(employee.getElementsByTagName("surname").item(0).getTextContent());
					empl.setUsername(username);
					return empl;
				}	
			}
			//Write to file
			DOMhelper.saveXMLContent(d, "src\\data\\employees.xml");
		}catch(Exception e){
			System.out.println("[EmployeeDOM]Error Finding an Employee!");
		}
		System.out.println("[EmployeeDOM]Failed to find an Employee!");
		return null;
	}
}
