package dom;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class AdministratorDOM {

	
	public boolean loginAdmin(String username, String password){
		try{
			Document d = DOMhelper.getDocument("src\\data\\administrators.xml");

			NodeList nl = d.getElementsByTagName("administrator");
			for(int i = 0; i<nl.getLength(); i++){
				Element book = (Element)nl.item(i);
				if(book.getElementsByTagName("username").item(0).getTextContent().equals(username) && book.getElementsByTagName("password").item(0).getTextContent().equals(password)){
					return true;
				}	
			}
			//Write to file
			DOMhelper.saveXMLContent(d, "src\\data\\administrators.xml");
		}catch(Exception e){
			System.out.println("[bookDOM]Error Finding an administrator!");
		}
		System.out.println("[bookDOM]Failed to find an administrator!");
		return false;
	}
}
