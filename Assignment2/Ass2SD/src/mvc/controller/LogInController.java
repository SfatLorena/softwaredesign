package mvc.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import mvc.model.AdministratorModel;
import mvc.model.EmployeeModel;
import mvc.model.LogInModel;
import mvc.view.AdminView;
import mvc.view.EmployeeView;
import mvc.view.LogInView;

public class LogInController {
	private LogInModel theModel;
	private LogInView theView;

	public LogInController(LogInModel theModel, LogInView theView) {
		this.theModel = theModel;
		this.theView = theView;

		this.theView.addLoginListener(new LoginListener());
	}

	class LoginListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {

			String username, password;

			username = theView.getUsername();
			password = theView.getPasswordField();

			theModel.setUsername(username);
			theModel.setPassword(password);

			if (theModel.checkLogIn() != null && theModel.checkLogIn().equals("admin")) {
				
				System.out.println("Logging in as admin!");
				
				AdministratorModel theModel = new AdministratorModel();
				AdminView theView = new AdminView();
				
				AdministratorController controller = new AdministratorController(theModel, theView);
				
				theView.setVisible(true);

				
			} else if (theModel.checkLogIn() != null && theModel.checkLogIn().equals("employee")) {
				System.out.println("Logging in as employee!");
				
				EmployeeModel em = new EmployeeModel();
				EmployeeView ev = new EmployeeView();
				
				EmployeeController controller = new EmployeeController(em,ev);
				
				ev.setVisible(true);
			}
			else{
				popupError("Wrong credentials!");
			}
			theView.dispose();
		}

	}
	
	private void popupError(String str) {
		JOptionPane.showMessageDialog(null, str, "Error", JOptionPane.ERROR_MESSAGE);
	}
}
