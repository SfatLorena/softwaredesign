package mvc.controller;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import mvc.model.AdministratorModel;
import mvc.model.BookModel;
import mvc.model.EmployeeModel;
import mvc.utils.CSVwriter;
import mvc.utils.OutputFactory;
import mvc.utils.PDFwriter;
import mvc.utils.Writer;
import mvc.view.AdminView;
import mvc.view.BookTableView;
import mvc.view.EmployeeTableView;

public class AdministratorController {
	private AdministratorModel theModel;
	private AdminView theView;

	public AdministratorController(AdministratorModel theModel, AdminView theView) {
		this.theModel = theModel;
		this.theView = theView;

		this.theView.addAddEmployeesListener(new ListenForAddEmployeesButton());
		this.theView.addDeleteEmployeesListener(new ListenForDeleteEmployeesButton());
		this.theView.addUpdateEmployeesListener(new ListenForUpdateEmployeesButton());
		this.theView.addViewEmployeesListener(new ListenForViewEmployeesButton());
		this.theView.addGenerateReportListener(new GenerateReportListener());

		this.theView.addAddBookListener(new ListenForAddBookButton());
		this.theView.addUpdateBookListener(new ListenForUpdateBookButton());
		this.theView.addViewBooksListener(new ListenForViewBooksButton());
		this.theView.addDeleteBookListener(new ListenForDeleteBookButton());
	}
	
	class GenerateReportListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			LinkedList<BookModel> books = theModel.getBooksOutOfStock();
			
			Writer writer;
			
			if(theView.rdbtnCsv.isSelected())
				writer = OutputFactory.OutputFactory("csv");
			else if(theView.rdbtnPdf.isSelected())
				writer = OutputFactory.OutputFactory("pdf");
			else
				writer = OutputFactory.OutputFactory("csv");
			
			writer.writeOutOfStockBooks(books);
			
			popupError("Raport generated!");
		}
		
	}

	class ListenForDeleteBookButton implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {

			JTextField id = new JTextField();
			final JComponent[] inputs = new JComponent[] { new JLabel("Book ID"), id };
			JOptionPane.showMessageDialog(null, inputs, "Delete Employee", JOptionPane.PLAIN_MESSAGE);
			System.out.println("Deleted book: " + id.getText());
			if (id.getText().equals("")) {
				popupError("Select a book to delete!");
			} else {
				System.out.println("~~~~~ "+checkNumber(id.getText()));
				if(checkNumber(id.getText()))
					theModel.deleteBook(id.getText());
				else
					popupError("Invalid identifier!");
			}

		}

	}

	class ListenForViewBooksButton implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			final LinkedList<BookModel> books = (LinkedList<BookModel>) theModel.viewAllBooks();
			// call class
			EventQueue.invokeLater(new Runnable() {
				public void run() {
					try {
						BookTableView view = new BookTableView(books);

						view.frame.setVisible(true);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

		}

	}

	class ListenForUpdateBookButton implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			JTextField id = new JTextField();
			JTextField price = new JTextField();
			JTextField quantity = new JTextField();
			final JComponent[] inputs = new JComponent[] { new JLabel("Book ID"), id, new JLabel("price"), price,
					new JLabel("quantity"), quantity };
			JOptionPane.showMessageDialog(null, inputs, "New Employee Info", JOptionPane.PLAIN_MESSAGE);

			String idd = id.getText();
			Double priceDbl;//
			int quantityDbl;// = Double.parseDouble(quantity.getText());

			if (!price.getText().equals(""))
				priceDbl = Double.parseDouble(price.getText());
			else
				priceDbl = (double) -1;
			if (!quantity.getText().equals(""))
				quantityDbl = Integer.parseInt(quantity.getText());
			else
				quantityDbl = -1;

			if (price.getText().equals("") && quantity.getText().equals(""))
				popupError("No update required!");

			else if (!checkNumber(price.getText()) || !checkNumber(quantity.getText()))
				popupError("Price and quantity must be numbers!");
			else if (priceDbl < 0 && (priceDbl != -1))
				popupError("No negative price allowed!");
			else if (quantityDbl < 0 && (quantityDbl != -1))
				popupError("No negative quantity allowed!");
			else {
				if (quantityDbl == -1)
					theModel.updateBook(idd + "", "", priceDbl + "");
				else if (priceDbl == -1)
					theModel.updateBook(idd + "", quantityDbl + "", "");
			}
		}

	}

	class ListenForAddBookButton implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			JTextField title = new JTextField();
			JTextField author = new JTextField();
			JTextField genre = new JTextField();
			JTextField price = new JTextField();
			JTextField quantity = new JTextField();
			final JComponent[] inputs = new JComponent[] {
					new JLabel("title"),
					title,
					new JLabel("author"),
					author,
					new JLabel("genre"),
					genre,
					new JLabel("price"),
					price,
					new JLabel("quantity"),
					quantity
			};
			JOptionPane.showMessageDialog(null, inputs, "New Employee Info", JOptionPane.PLAIN_MESSAGE);
			System.out.println("New book info: " +
					title.getText() + ", " +
					author.getText()+", "+genre.getText());
			
			String titleStr = title.getText();
			String authorStr = author.getText();
			String genreStr = genre.getText();
			Double priceDbl = null;// = Double.parseDouble(price.getText());
			int quantityDbl = 0;// = Integer.parseInt(quantity.getText());
			
			if(checkEmpty(titleStr) || checkEmpty(authorStr) || checkEmpty(genreStr)){
				popupError("Fields must not be empty!");
			}
			else if(!checkName(title.getText()) || !checkName(author.getText()) || !checkName(genre.getText()))
				popupError("Title, author and genre must contain only letters!");
			else if(!checkNumber(price.getText()) || !checkNumber(quantity.getText()))
					popupError("Price and quantity must be numbers!");
				else{
					try{
						priceDbl = Double.parseDouble(price.getText());
							quantityDbl = Integer.parseInt(quantity.getText());
							
							if (priceDbl<0)
								popupError("No negative price allowed!");
							if(quantityDbl<0)
								popupError("No negative quantity allowed!");
							else
								theModel.addBook( titleStr, authorStr, genreStr, ""+quantityDbl, priceDbl+"");
							
						
					}catch (NumberFormatException er) {
						popupError("Error parsing!");
					}
					
				}
			
		}

	}

	class ListenForViewEmployeesButton implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {

			final LinkedList<EmployeeModel> employees = (LinkedList<EmployeeModel>) theModel.viewEmployees();
			// call class
			EventQueue.invokeLater(new Runnable() {
				public void run() {
					try {
						EmployeeTableView view = new EmployeeTableView(employees);

						view.frame.setVisible(true);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
		}

	}

	class ListenForUpdateEmployeesButton implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			JTextField username = new JTextField();
			JTextField name = new JTextField();
			JTextField surname = new JTextField();
			JTextField address = new JTextField();
			final JComponent[] inputs = new JComponent[] { new JLabel("Select employee username"), username,
					new JLabel("New name"), name, new JLabel("New Surname"), surname, new JLabel("New Address"),
					address };
			JOptionPane.showMessageDialog(null, inputs, "Updated Employee Info", JOptionPane.PLAIN_MESSAGE);
			System.out.println(
					"Updated employee info: " + name.getText() + ", " + surname.getText() + ", " + address.getText());
			if (username.equals("") && name.equals("") && surname.equals("") && address.equals("")) {
				popupError("No changes made, no need for an update!");
			} else if (!checkName(name.getText()) || !checkName(surname.getText()))
				popupError("Names contain only letters!");
			else {
				theModel.updateEmployee(username.getText(), name.getText(), surname.getText(), address.getText());
				popupSuccess("Successfully epdated an employee!");
			}

		}
	}

	class ListenForDeleteEmployeesButton implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			JTextField username = new JTextField();
			final JComponent[] inputs = new JComponent[] { new JLabel("Username"), username };
			JOptionPane.showMessageDialog(null, inputs, "Delete Employee", JOptionPane.PLAIN_MESSAGE);
			System.out.println("Deleted employee: " + username.getText());
			if (username.getText().equals("")) {
				popupError("Select an employee to delete!");
			} else {
				if (!theModel.deleteEmployee((username.getText())))
					popupError("Failed to delete the employee!");
				else
					popupSuccess("Successfully deleted and employee!");
			}

		}

	}

	class ListenForAddEmployeesButton implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			JTextField name = new JTextField();
			JTextField surname = new JTextField();
			JTextField username = new JTextField();
			JTextField password = new JTextField();
			JTextField address = new JTextField();
			final JComponent[] inputs = new JComponent[] { new JLabel("Username"), username, new JLabel("Password"),
					password, new JLabel("Name"), name, new JLabel("Surname"), surname, new JLabel("Address"),
					address };
			JOptionPane.showMessageDialog(null, inputs, "New Employee Info", JOptionPane.PLAIN_MESSAGE);
			System.out.println(
					"New employee info: " + name.getText() + ", " + surname.getText() + ", " + address.getText());

			String nameStr = name.getText();
			String surnameStr = surname.getText();
			String usernameStr = username.getText();
			String passwordStr = password.getText();
			String addressStr = address.getText();
			if (checkEmpty(nameStr) || checkEmpty(surnameStr) || checkEmpty(usernameStr) || checkEmpty(passwordStr)
					|| checkEmpty(addressStr)) {
				popupError("Fields must not be empty!");
			} else if (!checkName(name.getText()) || !checkName(surname.getText()))
				popupError("Names contain only letters!");
			else {
				if (!theModel.searchEmployeeUsernameAvailability(username.getText())) {
					// Good to go!
					if (!theModel.createEmployee(usernameStr, passwordStr, nameStr, surnameStr, addressStr)) {
						popupError("Failed to add employee! Password not strong enough!");
					} else
						popupSuccess("Successfully added a new employee!");
				} else
					popupError("Username already taken!");
			}

		}
		
		
	}
	
	

	private boolean checkEmpty(String str) {
		if (str.equals(""))
			return true;
		else
			return false;
	}

	private void popupError(String str) {
		JOptionPane.showMessageDialog(null, str, "Error", JOptionPane.ERROR_MESSAGE);
	}

	private void popupSuccess(String str) {
		JOptionPane.showMessageDialog(null, str, "Done", JOptionPane.PLAIN_MESSAGE);
	}

	private boolean checkNumber(String str) {
		if (str.equals(""))
			return true;
		for (int i = 0; i < str.length(); i++) {
			if (!Character.isDigit(str.charAt(i)))
				if ((str.charAt(i) != '-') && (str.charAt(i) != '.'))
					return false;
		}
		return true;
	}

	private boolean checkName(String str) {
		for (int i = 0; i < str.length(); i++) {
			if (Character.isDigit(str.charAt(i)))
				return false;
		}
		return true;
	}

}
