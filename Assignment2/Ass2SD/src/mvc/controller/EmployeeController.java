package mvc.controller;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import mvc.model.BookModel;
import mvc.model.EmployeeModel;
import mvc.view.BookTableView;
import mvc.view.EmployeeTableView;
import mvc.view.EmployeeView;

public class EmployeeController {
	EmployeeModel theModel;
	EmployeeView theView;

	public EmployeeController(EmployeeModel theModel, EmployeeView theView) {
		this.theModel = theModel;
		this.theView = theView;

		ActionListener ac = new SearchByGenreListener();

		if (theView.rdbtnGenre.isSelected()) {
			ac = new SearchByGenreListener();
			System.out.println("Genre search selected.");
		} else if (theView.rdbtnAuthor.isSelected()) {
			ac = new SearchByAuthorListener();
			System.out.println("Author search selected.");
		} else if (theView.rdbtnTitle.isSelected()) {
			ac = new SearchByTitleListener();
			System.out.println("Title search selected.");
		}
		System.out.println("No search selected.");
		theView.addSearchListener(ac);
		theView.addSellListener(new SellListener());

	}

	class SellListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			JTextField id = new JTextField();
			JTextField amount = new JTextField();
			final JComponent[] inputs = new JComponent[] { new JLabel("Book ID:"), id, new JLabel("Quantity:"),
					amount };
			JOptionPane.showMessageDialog(null, inputs, "Selling Book", JOptionPane.PLAIN_MESSAGE);
			if (checkNumber(id.getText())) {
				if (theModel.getBook(id.getText()) == null) {
					popupError("No such book!");
				} else {
					if (checkNumber(amount.getText()) || Integer.parseInt(amount.getText()) <= 0) {
						theModel.sellBooksByID(Integer.parseInt(id.getText()), Integer.parseInt(amount.getText()));
						popupSuccess("Book(s) sold!");
					} else
						popupError("Not a valid quantity!");
				}
			}
		}

	}

	class SearchByTitleListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			final LinkedList<BookModel> bm = (LinkedList<BookModel>) theModel.viewBookbyTitle(theView.getTextField());

			if (bm == null || bm.isEmpty()) {
				popupError("No results!");
			} else
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							BookTableView view = new BookTableView(bm);

							view.frame.setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
		}

	}

	class SearchByAuthorListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			final LinkedList<BookModel> bm = (LinkedList<BookModel>) theModel.viewBookbyAuthor(theView.getTextField());
			if (bm == null || bm.isEmpty()) {
				popupError("No results!");
			} else
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							BookTableView view = new BookTableView(bm);

							view.frame.setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
		}

	}

	class SearchByGenreListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			final LinkedList<BookModel> bm = (LinkedList<BookModel>) theModel.viewBookbyGenre(theView.getTextField());
			if (bm == null || bm.isEmpty()) {
				popupError("No results!");
			} else
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							BookTableView view = new BookTableView(bm);

							view.frame.setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
		}

	}

	private void popupError(String str) {
		JOptionPane.showMessageDialog(null, str, "Error", JOptionPane.ERROR_MESSAGE);
	}

	private void popupSuccess(String str) {
		JOptionPane.showMessageDialog(null, str, "Done", JOptionPane.PLAIN_MESSAGE);
	}

	private boolean checkNumber(String str) {
		if (str.equals(""))
			return true;
		for (int i = 0; i < str.length(); i++) {
			if (!Character.isDigit(str.charAt(i)))
				if ((str.charAt(i) != '-') && (str.charAt(i) != '.'))
					return false;
		}
		return true;
	}
}
