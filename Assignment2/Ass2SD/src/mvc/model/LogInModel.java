package mvc.model;

import dom.AdministratorDOM;
import dom.EmployeeDOM;

public class LogInModel {
	private String username;
	private String password;
	private AdministratorDOM adom = new AdministratorDOM();
	private EmployeeDOM edom = new EmployeeDOM();
	
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String checkLogIn(){
		
		if( adom.loginAdmin(username, password))
			return "admin";
		else
			if(edom.loginEmployee(username, password))
				return "employee";
		else return null;
	}
}
