package mvc.model;

public class BookModel {
	//title, author, genre, quantity, and price
	private int ID;
	private String title;
	private String author;
	private String genre;
	private int quantity;
	private double price;
	

	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public int getID() {
		return ID;
	}
	public void setID(int iD) {
		ID = iD;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}

	@Override
	public String toString(){
		return "["+getID()+"] ''"+getTitle()+"'' by "+getAuthor()+" - genre: "+getGenre();
	}
}
