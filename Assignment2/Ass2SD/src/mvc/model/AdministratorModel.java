package mvc.model;

import java.util.LinkedList;
import java.util.List;

import dom.BookDOM;
import dom.EmployeeDOM;

public class AdministratorModel {
	private String username;
	private String password;
	private EmployeeDOM edom = new EmployeeDOM();
	private BookDOM bdom = new BookDOM();

	public List<EmployeeModel> returnAllEmployees() {
		return null;
	}

	public LinkedList<BookModel> getBooksOutOfStock(){
		return bdom.getBooksOutOfStock();
	}
	
	public void addBook(String titleIN, String authorIN, String genreIN, String quantityIN, String priceIN) {
		int id;

		if (bdom.getLastID() == null)
			id = 0;
		else
			id = Integer.parseInt(bdom.getLastID());

		bdom.addBook("" + (++id), titleIN, authorIN, genreIN, quantityIN, priceIN);
	}

	public void updateBook(String id, String quantityIN, String priceIN) {
		bdom.updateBook(id, quantityIN, priceIN);
	}

	public void deleteBook(String id) {
		bdom.deletebook(id);
	}

	public List<BookModel> viewAllBooks() {
		return bdom.viewAllbooks();
	}

	public EmployeeModel selectEmployee(String username, String password) {

		return null;
	}

	public void updateEmployee(String username, String name, String surname, String address) {
		if (!username.equals("") && !name.equals("") && !surname.equals("") && !address.equals(""))
			edom.updateEmployee(username, name, surname, address);
	}

	public boolean createEmployee(String username, String password, String name, String surname, String address) {
		if (securePassword(password)) {
			edom.addEmployee(username, password, name, surname, address);
			return true;
		}
		return false;
	}

	public List<EmployeeModel> viewEmployees() {
		return edom.viewAllEmployees();
	}

	public boolean deleteEmployee(String username) {
		return edom.deleteEmployee(username);
	}

	public EmployeeModel getEmployee(String username, String password) {
		return edom.getEmployee(username, password);
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public boolean searchEmployeeUsernameAvailability(String username) {
		return edom.searchEmployeeUsername(username);
	}

	public boolean securePassword(String password) {
		// Requirements: upper & lowercase letters, at least 1 digit,min 7
		// charasters

		boolean upper = false;
		boolean lower = false;
		boolean digit = false;
		int length = 0;

		for (char c : password.toCharArray()) {
			if (Character.isUpperCase(c))
				upper = true;
			else if (Character.isLowerCase(c))
				lower = true;
			else if (Character.isDigit(c))
				digit = true;

			length++;
		}

		if (!upper || !lower || !digit || (length < 7))
			return false;
		else
			return true;
	}
}
