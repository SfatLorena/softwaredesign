package mvc.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractButton;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;

public class AdminView extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JButton btnViewEmployees;
	private JButton btnAddEmployee;
	private JButton btnUpdateEmployee;
	private JButton btnDeleteEmployee;
	private JButton btnViewBooks, btnAddBook, btnUpdateBook, btnDeleteBook, btnGenerateReport;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	public JRadioButton rdbtnCsv, rdbtnPdf;

	/**
	 * Create the frame.
	 */
	public AdminView() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 376, 304);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setTitle("Administrator Menu");
		
		setContentPane(contentPane);
		
		
		
		btnViewEmployees = new JButton("View Employees");
		
		btnAddEmployee = new JButton("Add Employee");
		
		btnUpdateEmployee = new JButton("Update Employee");

		btnDeleteEmployee = new JButton("Delete Employee");

		btnGenerateReport = new JButton("Generate Report");

		JButton btnDone = new JButton("Done");
		btnDone.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		
		btnViewBooks = new JButton("View Books");
		
		btnAddBook = new JButton("Add Book");
		
		btnUpdateBook = new JButton("Update Book");
		
		btnDeleteBook = new JButton("Delete Book");
		
		rdbtnCsv = new JRadioButton("CSV");
		buttonGroup.add(rdbtnCsv);
		
		rdbtnPdf = new JRadioButton("PDF");
		buttonGroup.add(rdbtnPdf);
		
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(btnDeleteEmployee, GroupLayout.DEFAULT_SIZE, 131, Short.MAX_VALUE)
						.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
							.addComponent(btnUpdateEmployee, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addComponent(btnAddEmployee, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addComponent(btnViewEmployees, GroupLayout.DEFAULT_SIZE, 131, Short.MAX_VALUE)))
					.addGap(69)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING, false)
						.addComponent(btnDeleteBook, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(btnUpdateBook, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 122, Short.MAX_VALUE)
						.addComponent(btnAddBook, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(btnViewBooks, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
					.addContainerGap())
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(122)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(rdbtnCsv)
							.addPreferredGap(ComponentPlacement.RELATED, 23, Short.MAX_VALUE)
							.addComponent(rdbtnPdf))
						.addComponent(btnGenerateReport, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
					.addGap(107))
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap(156, Short.MAX_VALUE)
					.addComponent(btnDone)
					.addGap(137))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnViewEmployees)
						.addComponent(btnViewBooks))
					.addGap(8)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnAddEmployee)
						.addComponent(btnAddBook))
					.addGap(14)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnUpdateEmployee)
						.addComponent(btnUpdateBook))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnDeleteEmployee)
						.addComponent(btnDeleteBook))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(rdbtnPdf)
						.addComponent(rdbtnCsv))
					.addPreferredGap(ComponentPlacement.RELATED, 10, Short.MAX_VALUE)
					.addComponent(btnGenerateReport)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnDone)
					.addGap(5))
		);
		contentPane.setLayout(gl_contentPane);
	}
	

	
	public void addViewEmployeesListener(ActionListener listenForViewEmployeesButton){
		btnViewEmployees.addActionListener(listenForViewEmployeesButton);
	}
	public void addAddEmployeesListener(ActionListener listenForAddEmployeesButton){
		btnAddEmployee.addActionListener(listenForAddEmployeesButton);
	}
	public void addUpdateEmployeesListener(ActionListener listenForUpdateEmployeesButton){
		btnUpdateEmployee.addActionListener(listenForUpdateEmployeesButton);
	}
	public void addDeleteEmployeesListener(ActionListener listenForDeleteEmployeesButton){
		btnDeleteEmployee.addActionListener(listenForDeleteEmployeesButton);
	}
	public void addViewBooksListener(ActionListener listenForViewBooksButton){
		btnViewBooks.addActionListener(listenForViewBooksButton);
	}
	public void addAddBookListener(ActionListener listenForAddBookButton){
		btnAddBook.addActionListener(listenForAddBookButton);
	}
	public void addUpdateBookListener(ActionListener listenForUpdateBookButton){
		btnUpdateBook.addActionListener(listenForUpdateBookButton);
	}
	public void addDeleteBookListener(ActionListener listenForDeleteBookButton){
		btnDeleteBook.addActionListener(listenForDeleteBookButton);
	}
	public void addGenerateReportListener(ActionListener generateReportListener){
		btnGenerateReport.addActionListener(generateReportListener);
	}
}
