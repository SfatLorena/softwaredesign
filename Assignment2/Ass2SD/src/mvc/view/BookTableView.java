package mvc.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import mvc.model.BookModel;
import mvc.model.EmployeeModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle.ComponentPlacement;

public class BookTableView {

	public JFrame frame;
	private LinkedList<BookModel> books;
	private JTable table;

	/**
	 * Create the application.
	 */
	public BookTableView(List<BookModel> list) {

		books = (LinkedList<BookModel>) list;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 534, 316);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		final DefaultTableModel model = new DefaultTableModel();

		table = new JTable(model);
		table.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"ID", "Title", "Author", "Genre", "Price", "Quantity"
			}
		));
		
		int numCols = table.getModel().getColumnCount();

		if (books != null)
			for (int i = 0; i < books.size(); i++) {
				Object[] fila = new Object[numCols];
				fila[0] = books.get(i).getID();
				fila[1] = books.get(i).getTitle();
				fila[2] = books.get(i).getAuthor();
				fila[3] = books.get(i).getGenre();
				fila[4] = books.get(i).getPrice();
				fila[5] = books.get(i).getQuantity();
				((DefaultTableModel) table.getModel()).addRow(fila);
			}
		else
			System.out.println("!! Empty book list !!");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JButton btnDone = new JButton("Done");
		btnDone.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
			}
		});
		
		JScrollPane scrollPane = new JScrollPane();
		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 518, Short.MAX_VALUE)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap(190, Short.MAX_VALUE)
					.addComponent(btnDone, GroupLayout.PREFERRED_SIZE, 190, GroupLayout.PREFERRED_SIZE)
					.addGap(138))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 236, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(btnDone)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		

		scrollPane.setViewportView(table);
		frame.getContentPane().setLayout(groupLayout);

	}
}
