package mvc.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ButtonGroup;
import javax.swing.JTextField;
import javax.swing.JButton;

public class EmployeeView extends JFrame {

	private JPanel contentPane;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JTextField textField;
	JButton btnSearch, btnSell;
	public JRadioButton rdbtnGenre;
	public JRadioButton rdbtnTitle;
	public JRadioButton rdbtnAuthor;
	/**
	 * Create the frame.
	 */
	public EmployeeView() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 360, 257);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		
		
		
		
		setContentPane(contentPane);
		
		JLabel lblSearchBookBy = new JLabel("Search book by:");
		
		rdbtnGenre = new JRadioButton("genre");
		buttonGroup.add(rdbtnGenre);
		
		rdbtnTitle = new JRadioButton("title");
		buttonGroup.add(rdbtnTitle);
		
		rdbtnAuthor = new JRadioButton("author");
		buttonGroup.add(rdbtnAuthor);
		
		textField = new JTextField();
		textField.setColumns(10);
		
		btnSearch = new JButton("Search");
		
		btnSell = new JButton("Sell");
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(38)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(lblSearchBookBy)
							.addContainerGap())
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
								.addComponent(textField, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 266, Short.MAX_VALUE)
								.addGroup(gl_contentPane.createSequentialGroup()
									.addComponent(rdbtnGenre)
									.addGap(65)
									.addComponent(rdbtnTitle)
									.addPreferredGap(ComponentPlacement.RELATED, 48, Short.MAX_VALUE)
									.addComponent(rdbtnAuthor)))
							.addGap(30))))
				.addGroup(Alignment.TRAILING, gl_contentPane.createSequentialGroup()
					.addContainerGap(140, Short.MAX_VALUE)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(btnSearch)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(8)
							.addComponent(btnSell)
							.addPreferredGap(ComponentPlacement.RELATED, 8, GroupLayout.PREFERRED_SIZE)))
					.addGap(129))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblSearchBookBy)
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(rdbtnGenre)
						.addComponent(rdbtnTitle)
						.addComponent(rdbtnAuthor))
					.addGap(18)
					.addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(btnSearch)
					.addGap(18)
					.addComponent(btnSell)
					.addContainerGap(23, Short.MAX_VALUE))
		);
		contentPane.setLayout(gl_contentPane);
	}
	
	public String getTextField() {
		return textField.getText();
	}

	public void addSearchListener(ActionListener searchByTitleListener){

			btnSearch.addActionListener(searchByTitleListener);

	}
	
	public void addSellListener(ActionListener sellListener){
		btnSell.addActionListener(sellListener);
	}
	
}
