package mvc.utils;

public class OutputFactory {
	public static Writer OutputFactory(String str){
		if(str == null)
			return null;
		else if(str.equalsIgnoreCase("pdf"))
			return new PDFwriter();
		else if(str.equalsIgnoreCase("csv"))
			return new CSVwriter();
		return null;
	}
}
