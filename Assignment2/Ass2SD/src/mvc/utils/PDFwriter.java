package mvc.utils;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import mvc.model.BookModel;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.List;

public class PDFwriter implements Writer{

	public void writeOutOfStockBooks(List<BookModel> books){
		Document document = new Document();
		try {
			PdfWriter.getInstance(document, new FileOutputStream("OutOfStock.pdf"));
		
			document.open();
			
			Paragraph paragraph = new Paragraph();
			
			for(BookModel book:books){
				
				paragraph.add(book.toString());
				document.add(paragraph);
				
			}
			document.close();
			
			
		} catch (FileNotFoundException e) {
			System.out.println("[ERROR] File not found!");
			e.printStackTrace();
		} catch (DocumentException e) {
			System.out.println("[ERROR] Doc exception!");
			e.printStackTrace();
		}
	}
}
