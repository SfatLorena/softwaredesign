package data.source.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import business.layer.models.Account;

public class GenericDAO<E> extends SessionImpl{
	
	public void add(E entity){

		openSession();
		System.out.println("Session Opened for ADD.");
		 	session.save(entity);
		 	System.out.println("Session Closed for ADD.");
		closeSession();
	}
	
	public void delete(E entity){

		openSession();
		
		 	session.delete(entity);
        
		closeSession();
	}
	
	public void update(E entity){

		openSession();
		
		 	session.saveOrUpdate(entity);;
        
		closeSession();
	}

}
