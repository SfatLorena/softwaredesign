package data.source.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import business.layer.models.Account;
import business.layer.models.Activity;
import business.layer.models.Employee;

public class ActivityDAO extends GenericDAO<Activity>{
	public List<Activity> view(){
		
		openSession();
		
			String q = "FROM Activity";
			org.hibernate.Query query = session.createQuery(q);
			List<Activity> list = query.list();
			
			for(int i = 0; i< list.size();i++)
			{
				System.out.println(list.get(i));
			}

		closeSession();
	
		return list;
	}
	
	public boolean searchByKey(Long idactivity){
		boolean ok=true;
		
		openSession();
		
		String q = "FROM Activity";
		org.hibernate.Query query = session.createQuery(q);
		List<Activity> list = query.list();
		
		
		ok = list.contains(session.get(Activity.class, idactivity));
		
		closeSession();
		
		return ok;
	}

	public List<Activity> searchForEmployee(Employee employee){
		openSession();
		
		
		Criteria cr = session.createCriteria(Activity.class);
		cr.add(Restrictions.eq("employee", employee));
		
		List<Activity> list =cr.list();
		
		closeSession();
		
		return list;
	}
	
	
	//********************* Validations ****************************
	
		public boolean okToAdd(Activity activity){
			boolean ok = true;
			
			if(searchByKey(activity.getIdactivity()))
				ok = false;
			if(activity.getEmployee() == null)
				ok = false;

			return ok;
		}
		
		public boolean okToDelete(Activity activity){
			boolean ok = false;
			
			if(searchByKey(activity.getIdactivity()))
				ok = true;
			
			return ok;
		}
		
}
