package data.source.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import business.layer.models.Account;
import business.layer.models.Activity;
import business.layer.models.Client;

public class AccountDAO extends SessionImpl{
	
	public boolean add(Account entity){

		if(okToAdd(entity)){
			openSession();
			System.out.println("Session Opened for ADD.");
			 	session.save(entity);
			 	System.out.println("Session Closed for ADD.");
			closeSession();
			return true;
		}
		return false;
	}
	
	public void delete(Account entity){

		openSession();
		
		 	session.delete(entity);
        
		closeSession();
	}
	
	public void update(Account entity){

		openSession();
		
		 	session.saveOrUpdate(entity);;
        
		closeSession();
	}

	
	@SuppressWarnings("unchecked")
	public List<Account> view(){
		
		List<Account> accounts;

		openSession();
		
		 	accounts = (List<Account>) 
				 session.createCriteria(Account.class).addOrder( Order.desc("creation_date")).list();
		 	
		 	for(int i = 0; i<accounts.size(); i++){
				System.out.println(accounts.get(i));
			}
			        
		closeSession();
	
		return accounts;
	}
	
	public boolean searchByKey(Long idaccount){
		boolean ok=true;
		
		openSession();
		
		String q = "FROM Account";
		org.hibernate.Query query = session.createQuery(q);
		List<Account> list = query.list();
		
		
		ok = list.contains(session.get(Account.class, idaccount));
		
		closeSession();
		
		return ok;
	}
	
	public List<Account> searchForClient(Client client){
		
		openSession();
		
		Criteria cr = session.createCriteria(Account.class);
		cr.add(Restrictions.eq("idclient", client.getCNP()));
		
		List<Account> list =cr.list();
		
		closeSession();
		
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public Account getByID(long ID){
			openSession();
			Account account = null;
			
			Criteria cr = session.createCriteria(Account.class);
			List<Account> accounts =  (List<Account>) cr.add(Restrictions.eq("idaccount", ID)).list();
			
			if(accounts != null)
				account = (Account) accounts.get(0);
			closeSession();
			
		return account;
	}
		
	

	
	//********************* Validations ****************************
	
	public boolean validAmount(double amount){
		if(amount>0)
			return true;
		else
			return false;
	}
	
	
	public boolean okToAdd(Account account){
		boolean ok = true;
		
		if(searchByKey(account.getIdaccount()))
			ok = false;
		if(account.getAmmount() < 0)
			ok = false;
		//account number = 16 digits
		if(String.valueOf(account.getIdaccount()).length() != 16)
			ok = false;
		if(account.getClient() == null)
			ok = false;
		return ok;
	}
	
	public boolean okToDelete(Account account){
		boolean ok = false;
		
		if(searchByKey(account.getIdaccount()))
			ok = true;
		
		return ok;
	}
	
	public boolean okToUpdate(Account account){
		boolean ok = true;
		
		if(account.getAmmount() < 0)
			ok = false;
		return ok;
	}
}
