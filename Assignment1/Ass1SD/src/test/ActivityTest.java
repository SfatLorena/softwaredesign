package test;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Test;

import business.layer.models.Activity;
import business.layer.models.Employee;
import data.source.dao.ActivityDAO;
import data.source.dao.EmployeeDAO;

public class ActivityTest {

	@Test
	public void test() {
		Activity activity = new Activity();
		activity.setName("Test");
		activity.setDate(new Date());
		activity.setDescription("Test");
		
		ActivityDAO ad = new ActivityDAO();
		
		Employee employee = new Employee();
		employee.setName("Test");
		employee.setSalary(0);
		employee.setSurname("Test");
		employee.setAddress("Test");
		employee.setPassword("1234");
		employee.setUsername("Test");
		
		activity.setEmployee(employee);
		
		EmployeeDAO ed = new EmployeeDAO();
		
		ed.add(employee);
		ad.add(activity);
		
		assertEquals(ad.searchByKey(activity.getIdactivity()),true);
		
		ad.delete(activity);
		ed.delete(employee);

		assertEquals(ad.searchByKey(activity.getIdactivity()),false);

	}

}
