package test;

import static org.junit.Assert.*;

import org.junit.Test;

import business.layer.models.Client;
import data.source.dao.ClientDAO;

public class ClientTest {
	@Test
	public void testAdd() {
		
		Client client = new Client();
		client.setName("Drule");
		client.setSurname("Denisa");
		client.setCNP("12343655446");
		client.setAddress("-");
		
		ClientDAO cd = new ClientDAO();
		
		assertTrue(cd.searchByKey(client.getCNP()) == false);
		
		cd.add(client);		
		
		assertTrue(cd.searchByKey(client.getCNP()) == true);
		
		cd.delete(client);
	}
	
	@Test
	public void testDelete() {
		
		Client client = new Client();
		client.setName("Drule");
		client.setSurname("Denisa");
		client.setCNP("12343655446");
		client.setAddress("-");
		
		ClientDAO cd = new ClientDAO();
				
		cd.add(client);		
		
		assertTrue(cd.searchByKey(client.getCNP()) == true);
		System.out.println("Get::::"+cd.getByKey(client.getCNP()));
		
		cd.delete(client);
				
		assertTrue(cd.searchByKey(client.getCNP()) == false);
	}
	
	@Test
	public void testUpdate(){
		
		Client clientTest = new Client();
		clientTest.setName("Drule");
		clientTest.setSurname("Denisa");
		clientTest.setCNP("12343655446");
		clientTest.setAddress("-");
		
		ClientDAO cd = new ClientDAO();
		
		cd.add(clientTest);	
		
		Client client = cd.getByKey("12343655446");
		System.out.println("TestUpdate:::"+client);
		assertTrue(cd.searchByKey("12343655446") == true);
		
		client.setName("Stanza");
		cd.update(client);
		
		Client clientAfter = cd.getByKey(client.getCNP());
		
		assertTrue(client.equals(clientAfter));
		
		
		cd.delete(client);
	}
	
	@Test
	public void validCNPtest(){
		ClientDAO cd = new ClientDAO();
		assertTrue(cd.validCNP("2940103314004"));
	}
}
