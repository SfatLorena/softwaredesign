package test;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.LinkedList;

import org.junit.Test;

import business.layer.models.Account;
import business.layer.models.Client;
import data.source.dao.AccountDAO;
import data.source.dao.ClientDAO;

public class AccountTest {

	@Test
	public void testDelete() {
						
		AccountDAO ad = new AccountDAO();
		ClientDAO cd = new ClientDAO();
		Client client = new Client();
		client.setName("Test");
		client.setSurname("Test");
		client.setCNP("12343655447");
		client.setAddress("-");
		cd.add(client);
				
		Account account = new Account();
		account.setAmmount(200);
		account.setCreation_date();
		account.setClient(client);
				
		ad.add(account);
		
		assertEquals(ad.searchByKey(account.getIdaccount()), true);
		System.out.println("1. "+ad.searchByKey(account.getIdaccount()));
		
		ad.delete(account);
		
		cd.delete(client);
		
		assertEquals(ad.searchByKey(account.getIdaccount()), false);
		System.out.println("2. "+ad.searchByKey(account.getIdaccount()));
		ad.view();
	}

}
