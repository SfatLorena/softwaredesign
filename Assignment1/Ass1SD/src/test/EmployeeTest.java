package test;

import static org.junit.Assert.*;

import org.junit.Test;

import business.layer.models.Employee;
import data.source.dao.EmployeeDAO;

public class EmployeeTest {

	@Test
	public void test() {
		Employee employee = new Employee();
		employee.setName("Test");
		employee.setSalary(0);
		employee.setSurname("Test");
		employee.setAddress("Test");
		employee.setPassword("1234");
		employee.setUsername("Test");
		
		EmployeeDAO ed = new EmployeeDAO();
		
		assertEquals(ed.searchByKey(employee.getUsername()),false);
		
		ed.add(employee);
		ed.view();
		
		assertEquals(ed.searchByKey(employee.getUsername()),true);
		
		ed.delete(employee);

		assertEquals(ed.searchByKey(employee.getUsername()),false);
		
		ed.view();
	}

}
