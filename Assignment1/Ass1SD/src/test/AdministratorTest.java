package test;

import static org.junit.Assert.*;

import org.junit.Test;

import business.layer.models.Administrator;
import data.source.dao.AdministratorDAO;

public class AdministratorTest {

	@Test
	public void test() {
		Administrator admin = new Administrator();
		admin.setUsername("Test");
		admin.setName("Test");
		admin.setPassword("1234");
		admin.setSurname("Test");
		admin.setAddress("Test");
		
		AdministratorDAO ad = new AdministratorDAO();
		
		assertTrue(ad.searchByKey(admin.getUsername()) == false);
		
		ad.add(admin);
		
		assertTrue(ad.searchByKey(admin.getUsername()) == true);
		
		System.out.println("Searched by key: "+ad.getbyKey(admin.getUsername()));
		
		ad.delete(admin);
		
		assertTrue(ad.searchByKey(admin.getUsername()) == false);
		
		ad.view();
	}

}
