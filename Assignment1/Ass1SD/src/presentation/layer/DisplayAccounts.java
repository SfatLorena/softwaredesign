package presentation.layer;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import business.layer.models.Account;
import business.layer.models.Client;
import data.source.dao.AccountDAO;
import data.source.dao.ClientDAO;

import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;

public class DisplayAccounts extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private final DefaultTableModel model = new DefaultTableModel();

	/**
	 * Create the frame.
	 */
	public DisplayAccounts() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 383);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JScrollPane scrollPane = new JScrollPane();
		
		JButton btnDone = new JButton("Done");
		btnDone.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 426, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap(186, Short.MAX_VALUE)
					.addComponent(btnDone)
					.addGap(183))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 291, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, 21, Short.MAX_VALUE)
					.addComponent(btnDone))
		);
		
		table = new JTable(model);
		table.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"ID", "Client", "amount (LEI)"
			}
		));
		
		AccountDAO ed = new AccountDAO();
		ArrayList<Account> account = (ArrayList<Account>) ed.view();
		
		int numCols = table.getModel().getColumnCount();
		
		for(int i = 0; i<account.size(); i++){
			Object [] fila = new Object[numCols];
			fila[0] = account.get(i).getIdaccount();
			fila[1] = account.get(i).getClient().getName()+" "+account.get(i).getClient().getSurname();
			fila[2] = account.get(i).getAmmount();
					
			((DefaultTableModel) table.getModel()).addRow(fila);
		}	
		scrollPane.setViewportView(table);
		contentPane.setLayout(gl_contentPane);
		setTitle("Accounts Table");
	}

}
