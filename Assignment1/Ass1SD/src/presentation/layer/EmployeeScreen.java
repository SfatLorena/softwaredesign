package presentation.layer;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import business.layer.models.Account;
import business.layer.models.Client;
import business.layer.models.Employee;
import business.layer.service.EmployeeService;
import data.source.dao.AccountDAO;

import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JComponent;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class EmployeeScreen extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private EmployeeService employeeServ = new EmployeeService();

	/**
	 * Create the frame.
	 */
	public EmployeeScreen(Employee empl) {
		
		employeeServ.setEmployee(empl);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 462, 232);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setTitle("Employee Menu");
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnViewClients = new JButton("View Clients");
		btnViewClients.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							DisplayClients frame = new DisplayClients();
							frame.setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			}
		});
		btnViewClients.setBounds(9, 11, 114, 23);
		contentPane.add(btnViewClients);
		
		JButton btnViewAccounts = new JButton("View Accounts");
		btnViewAccounts.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							DisplayAccounts frame = new DisplayAccounts();
							frame.setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			}
		});
		btnViewAccounts.setBounds(320, 11, 114, 23);
		contentPane.add(btnViewAccounts);
		
		JButton btnAddClient = new JButton("Add Client");
		btnAddClient.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JTextField name = new JTextField();
				JTextField surname = new JTextField();
				JTextField CNP = new JTextField();
				JTextField address = new JTextField();
				final JComponent[] inputs = new JComponent[] {
						new JLabel("First"),
						name,
						new JLabel("Last"),
						surname,
						new JLabel("CNP"),
						CNP,
						new JLabel("Address"),
						address
				};
				JOptionPane.showMessageDialog(null, inputs, "New Client Info", JOptionPane.PLAIN_MESSAGE);
				System.out.println("New client info: " +
						name.getText() + ", " +
						surname.getText()+", "+CNP.getText()+", "+address.getText());
				
				if(name.getText().equals("") || surname.getText().equals("") ||
						CNP.getText().equals("") || address.getText().equals(""))
					popupError("Field must not be empty!");
				else if(!checkName(name.getText()) || !checkName(surname.getText()))
					popupError("Names contain only letters!");
				else if(!checkNumber(CNP.getText()))
					popupError("CNP must only contain numbers!");
				else{
					if(employeeServ.addClient(address.getText(), CNP.getText(), name.getText(), surname.getText()))
						popupSuccess("Successfully added a client!");
					else
						popupError("Failed to add a client!");
				}
			}
		});
		btnAddClient.setBounds(9, 45, 114, 23);
		contentPane.add(btnAddClient);
		
		JButton btnDeleteClient = new JButton("Delete Client");
		btnDeleteClient.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JTextField CNP = new JTextField();
				final JComponent[] inputs = new JComponent[] {
						new JLabel("Input client's CNP"),
						CNP
				};
				JOptionPane.showMessageDialog(null, inputs, "CNP:", JOptionPane.PLAIN_MESSAGE);
				System.out.println("Client to delete: "+CNP.getText());
				
				if(checkEmpty(CNP.getText()))
					popupError("Invalid CNP!");
				else{
					if(employeeServ.deleteClient(CNP.getText()))
						popupSuccess("Successfully deleted a client!");
					else
						popupError("Failed to delete a client!");
				}
				
			}
		});
		btnDeleteClient.setBounds(9, 79, 114, 23);
		contentPane.add(btnDeleteClient);
		
		JButton btnAddAccount = new JButton("Add Account");
		btnAddAccount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JTextField amount = new JTextField();
				JTextField clientCNP = new JTextField();
				JTextField accountNR = new JTextField();
				final JComponent[] inputs = new JComponent[] {
						new JLabel("Client CNP:"),
						clientCNP,
						new JLabel("Set account ID:"),
						accountNR,
						new JLabel("Initial deposit:"),
						amount
				};
				JOptionPane.showMessageDialog(null, inputs, "New Account", JOptionPane.PLAIN_MESSAGE);
				System.out.println("New account info: " +
						clientCNP.getText() + " - "+ amount.getText()+" lei.");
				
				if(checkEmpty(amount.getText()) || checkEmpty(clientCNP.getText()) || checkEmpty(accountNR.getText()))
					popupError("Fields must not be empty!");
				if(!checkNumber(amount.getText()) || !checkNumber(clientCNP.getText()) || !checkNumber(accountNR.getText()))
					popupError("Fields must only contain digits!");
				else{
					System.out.println("All in!");
					if(employeeServ.getCd().searchByKey(clientCNP.getText())){
						System.out.println("All in2!");

						Client client = employeeServ.getCd().getByKey((clientCNP.getText()));
						System.out.println("All in4!");

						if(employeeServ.addAccount(Double.parseDouble(amount.getText()), client, Long.parseLong(accountNR.getText()))){
							System.out.println("All in3!");

							popupSuccess("New account has been added!");
						}
						else
							popupError("Failed to add new account!");
					}
					else
						popupError("No such client!");
				}
			}
		});
		btnAddAccount.setBounds(320, 45, 114, 23);
		contentPane.add(btnAddAccount);
		
		JButton btnDeleteAccount = new JButton("Delete Account");
		btnDeleteAccount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JTextField ID = new JTextField();
				final JComponent[] inputs = new JComponent[] {
						new JLabel("Input account identifier:"),
						ID
				};
				JOptionPane.showMessageDialog(null, inputs, "Delete Account", JOptionPane.PLAIN_MESSAGE);
				System.out.println("Account deleted: "+ID.getText());
				
				if(employeeServ.getAd().searchByKey(Long.parseLong(ID.getText()))){
					if(employeeServ.deleteAccount(Long.parseLong(ID.getText())))
						popupSuccess("Successfully deleted an account!");
					else
						popupError("Failed to delete an account!");
				}
				else
					popupError("No such account!");
					
			}
		});
		btnDeleteAccount.setBounds(320, 79, 114, 23);
		contentPane.add(btnDeleteAccount);
		
		JButton btnTransferMoney = new JButton("Transfer Money");
		btnTransferMoney.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JTextField ID1 = new JTextField();
				JTextField ID2 = new JTextField();
				JTextField amount = new JTextField();
				final JComponent[] inputs = new JComponent[] {
						new JLabel("Input 1st account identifier:"),
						ID1,
						new JLabel("Input 2nd account identifier:"),
						ID2,
						new JLabel("How much?"),
						amount
				};
				JOptionPane.showMessageDialog(null, inputs, "Money Transfer:", JOptionPane.PLAIN_MESSAGE);
				System.out.println("Money transferred from: "+ID1.getText()+" to "+ID2.getText());
				
				if(checkEmpty(ID1.getText()) || checkEmpty(ID2.getText()) || !checkNumber(ID1.getText()) || !checkNumber(ID2.getText()))
					popupError("Fields must not be empty of contain anything but digits!");
				else{
					Account acc1 = employeeServ.getAccountByID((Long.parseLong(ID1.getText())));
					Account acc2 = employeeServ.getAccountByID((Long.parseLong(ID2.getText())));
					
					int result = JOptionPane.showConfirmDialog(null, "Are you sure you want to transfer "+
					amount.getText()+" lei from "+acc1.getClient().getName()+" "+acc1.getClient().getSurname()+" to "+
							acc2.getClient().getName()+" "+acc2.getClient().getSurname()+"?", "Confirmation",
			                JOptionPane.YES_NO_OPTION);
					
					if(result == JOptionPane.YES_OPTION)
						if(employeeServ.transferMoney(Long.parseLong(ID1.getText()), Long.parseLong(ID2.getText()), 
							Double.parseDouble(amount.getText())))
							popupSuccess("Transfer succesful!");
						else
							popupError("Transfer failed!");
				}
			}
		});
		btnTransferMoney.setBounds(151, 45, 140, 23);
		contentPane.add(btnTransferMoney);
		
		JButton btnDone = new JButton("Done");
		btnDone.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnDone.setBounds(179, 160, 89, 23);
		contentPane.add(btnDone);
		
		JButton btnUpdateClient = new JButton("Update Client");
		btnUpdateClient.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JTextField name = new JTextField();
				JTextField surname = new JTextField();
				JTextField CNP = new JTextField();
				JTextField address = new JTextField();
				final JComponent[] inputs = new JComponent[] {
						new JLabel("CNP"),
						CNP,
						new JLabel("New First name"),
						name,
						new JLabel("New Last name"),
						surname,
						new JLabel("New Address"),
						address
				};
				JOptionPane.showMessageDialog(null, inputs, "Update Client Info", JOptionPane.PLAIN_MESSAGE);
				System.out.println("New client info: " +
						name.getText() + ", " +
						surname.getText()+", "+CNP.getText()+", "+address.getText());
				if(!checkName(name.getText()) || !checkName(surname.getText()))
					popupError("Names contain only letters!");
				else if(!checkNumber(CNP.getText()))
					popupError("CNP must only contain numbers!");
				else if(!employeeServ.getCd().searchByKey(CNP.getText()))
					popupError("No such client!");
				else{
					employeeServ.updateClient(address.getText(), name.getText(), surname.getText(), CNP.getText());
					popupSuccess("Client Updated!");
				}
			}
		});
		btnUpdateClient.setBounds(9, 113, 114, 23);
		contentPane.add(btnUpdateClient);
		
		JButton btnUpdateAccount = new JButton("Update Account");
		btnUpdateAccount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
					JTextField amount = new JTextField();
					JTextField accountNR = new JTextField();
					final JComponent[] inputs = new JComponent[] {
							new JLabel("Input account ID:"),
							accountNR,
							new JLabel("New deposit sold:"),
							amount
					};
					JOptionPane.showMessageDialog(null, inputs, "Update Account", JOptionPane.PLAIN_MESSAGE);
					System.out.println("New account info: " + " - "+ amount.getText()+" lei.");
					
					if(checkEmpty(amount.getText()) || checkEmpty(accountNR.getText()))
						popupError("Fields must not be empty!");
					if(!checkNumber(amount.getText()) || !checkNumber(accountNR.getText()))
						popupError("Fields must only contain digits!");
					else{
						employeeServ.updateAccount(Long.parseLong(accountNR.getText()), Double.parseDouble(amount.getText()));
						popupSuccess("Account updated!");
					}
			}
		});
		btnUpdateAccount.setBounds(320, 113, 114, 23);
		contentPane.add(btnUpdateAccount);
	}
	
	private boolean checkEmpty(String str){
		if(str.equals(""))
			return true;
		else
			return false;
	}
	
	private boolean checkNumber(String str){
		for (int i = 0; i < str.length(); i++) {
		      if (!Character.isDigit(str.charAt(i)))
		        return false;
		    }
		    return true;
	}
	
	private boolean checkName(String str){
		for (int i = 0; i < str.length(); i++) {
		      if (Character.isDigit(str.charAt(i)))
		        return false;
		    }
		    return true;
	}
	
	private void popupError(String str){
		JOptionPane.showMessageDialog(null, str, "Error",
                JOptionPane.ERROR_MESSAGE);
	}
	
	private void popupSuccess(String str){
		JOptionPane.showMessageDialog(null, str, "Done",
                JOptionPane.PLAIN_MESSAGE);
	}
}
