package business.layer.service;

import java.util.Date;
import java.util.List;

import business.layer.models.Account;
import business.layer.models.Activity;
import business.layer.models.Client;
import business.layer.models.Employee;
import data.source.dao.AccountDAO;
import data.source.dao.ActivityDAO;
import data.source.dao.ClientDAO;

public class EmployeeService {
	
	Employee employee;
	ClientDAO cd = new ClientDAO();
	AccountDAO ad = new AccountDAO();
	ActivityDAO actd = new ActivityDAO();
	
	public Employee getEmployee() {
		return employee;
	}
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	
	public ActivityDAO getActd() {
		return actd;
	}
	public void setActd(ActivityDAO actd) {
		this.actd = actd;
	}
	public AccountDAO getAd() {
		return ad;
	}

	public void setAd(AccountDAO ad) {
		this.ad = ad;
	}

	public ClientDAO getCd() {
		return cd;
	}

	public void setCd(ClientDAO cd) {
		this.cd = cd;
	}

	public boolean addClient(String address, String CNP, String name, String surname){
		
		Client client = new Client();
		client.setAddress(address);
		client.setCNP(CNP);
		client.setName(name);
		client.setSurname(surname);
		
		if(cd.add(client)){
		
			//Activity
			Activity activ = new Activity();
			activ.setDate(new Date());
			activ.setEmployee(employee);
			activ.setDescription("Added a new client - "+client.toString());
			activ.setName("New Client");
			actd.add(activ);
			
			return true;
		}
		return false;
	}
	
	public boolean deleteClient(String CNP){
		
		Client client = cd.getByKey(CNP);
		cd.delete(client);
		
		//Activity
				Activity activ = new Activity();
				activ.setDate(new Date());
				activ.setEmployee(employee);
				activ.setDescription("Deleted a client - "+client.toString());
				activ.setName("Delete Client");
				actd.add(activ);
		
		return !cd.searchByKey(client.getCNP());
	}
	
	public String viewClient(String CNP){
		return cd.getByKey(CNP).toString();
	}
	
	public List<Account> viewAccounts(Client client){
		return ad.searchForClient(client);
	}
	
	public Account getAccountByID(Long id){
		return ad.getByID(id);
	}
	
	public void updateClient(String address, String name, String surname, String CNP){
		Client client = cd.getByKey(CNP);
		
		boolean change = false;
		
		if(!address.equals("")){
			client.setAddress(address);
			change = true;
		}
		if(!name.equals("")){
			client.setName(name);
			change = true;
		}
		if(!surname.equals("")){
			client.setSurname(surname);
			change = true;
		}
		
		if(change)
			cd.update(client);
		
		//Activity
				Activity activ = new Activity();
				activ.setDate(new Date());
				activ.setEmployee(employee);
				activ.setDescription("Updated a client - "+client.toString());
				activ.setName("Update Client");
				actd.add(activ);
		
	}
	
	public boolean addAccount(double amount, Client client, Long number){
		Account account = new Account();
		account.setIdaccount(number);
		account.setAmmount(amount);	
		account.setClient(client);
		account.setCreation_date();
		
		if(ad.add(account)){
		
		//Activity
				Activity activ = new Activity();
				activ.setDate(new Date());
				activ.setEmployee(employee);
				activ.setDescription("Added a new account-"+account.toString());
				activ.setName("New Account");
				actd.add(activ);
				return true;
		}
		return false;
	}
	
	public void updateAccount(long ID, double amount){
		
		Account account = ad.getByID(ID);
		account.setAmmount(amount);
		
		ad.update(account);
		
		//Activity
				Activity activ = new Activity();
				activ.setDate(new Date());
				activ.setEmployee(employee);
				activ.setDescription("Updated an account - "+account.toString());
				activ.setName("Update Account");
				actd.add(activ);
	}
	
	public boolean deleteAccount(long ID){
		Account account = ad.getByID(ID);
		ad.delete(account);
		
		//Activity
		Activity activ = new Activity();
		activ.setDate(new Date());
		activ.setEmployee(employee);
		activ.setDescription("Deleted an account - "+account.toString());
		activ.setName("Delete Account");
		actd.add(activ);
		
		return !ad.searchByKey(ID);
	}
	
	public String viewAccount(long ID){
		return ad.getByID(ID).toString();
	}
	
	public boolean transferMoney(long ID1, long ID2, double amount){
		
		if(amount <= 0 ) return false;
		double init1, init2;
		Account acc1 = ad.getByID(ID1);

		init1 = acc1.getAmmount();
		if (init1-amount < 0) return false;
		
		acc1.setAmmount(acc1.getAmmount() - amount);
		ad.update(acc1);
		
		Account acc2 = ad.getByID(ID2);
		init2 = acc2.getAmmount();
		acc2.setAmmount(acc2.getAmmount() + amount);
		ad.update(acc2);
		
		
		//Activity
		Activity activ = new Activity();
		activ.setDate(new Date());
		activ.setEmployee(employee);
		activ.setDescription("Money Transfer - "+acc1.toString()+"(init:"+ init1+"; now:"+acc1.getAmmount()+
				") to "+acc2.toString()+"(init:"+ init2+"; now:"+acc2.getAmmount()+")");
		activ.setName("Money Transfer");
		actd.add(activ);
		
		if(ad.getByID(ID2).getAmmount()!= init2 && ad.getByID(ID1).getAmmount()!=init1)
			return true;
		else return false;
	}

}
