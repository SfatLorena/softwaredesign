package business.layer.service;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import business.layer.models.Activity;
import business.layer.models.Administrator;
import business.layer.models.Employee;
import data.source.dao.ActivityDAO;
import data.source.dao.AdministratorDAO;
import data.source.dao.EmployeeDAO;

public class AdministratorService {
	
	EmployeeDAO ed = new EmployeeDAO();
	ActivityDAO ad = new ActivityDAO();
	AdministratorDAO admind = new AdministratorDAO();
	public AdministratorDAO getAdmind() {
		return admind;
	}

	public void setAdmind(AdministratorDAO admind) {
		this.admind = admind;
	}

	Administrator administrator;

	public Administrator getAdministrator() {
		return administrator;
	}

	public void setAdministrator(Administrator administrator) {
		this.administrator = administrator;
	}

	public ActivityDAO getAd() {
		return ad;
	}

	public void setAd(ActivityDAO ad) {
		this.ad = ad;
	}

	public EmployeeDAO getEd() {
		return ed;
	}

	public void setEd(EmployeeDAO ed) {
		this.ed = ed;
	}
	
	public boolean addAdmin(String username, String password, String name, String surname, String address){
		Administrator admin = new Administrator();
		admin.setUsername(username);
		admin.setPassword(password);
		admin.setName(name);
		admin.setSurname(surname);
		admin.setAddress(address);
		
		admind.add(admin);
		
		if(admind.searchByKey(username))				
			return true;
		else
			return false;
	}
	
	public boolean addEmployee(String username, String password, String name, String surname, String address, double salary){
		Employee employee = new Employee();
		employee.setUsername(username);
		employee.setName(name);
		employee.setSurname(surname);
		employee.setPassword(password);
		employee.setAddress(address);
		employee.setSalary(salary);
		
		ed.add(employee);
		return ed.searchByKey(employee.getUsername());
	}
	
	public boolean deleteEmployee(String username){
		
		Employee employee = ed.getByKey(username);
		
		ed.delete(employee);
		
		if(ed.searchByKey(employee.getUsername()))
			return false;
		return true;
	}
	
	public void updateEmployee(String username, String name, String surname, String address, double salary){
		Employee employee = ed.getByKey(username);
		
		boolean change = false;
		
		if(!address.equals("") && !address.equals(employee.getAddress())){
			employee.setAddress(address);
			change = true;
		}
		if(!name.equals("") && !name.equals(employee.getName())){
			employee.setName(name);
			change = true;
		}
		if(!surname.equals("") && !surname.equals(employee.getSurname())){
			employee.setSurname(surname);
			change = true;
		}
		if(salary != 0 && salary != employee.getSalary()){
			employee.setSalary(salary);
			change = true;
		}
		
		if(change) ed.update(employee);
	}
	
	public List<Activity> viewReport(Employee employee){
		List<Activity> activities =  ad.searchForEmployee(employee);

		return activities;
	}

}
